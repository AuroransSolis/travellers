use game::GameInfo;

fn main() {
    let start = std::time::Instant::now();
    let gameinfo = GameInfo::init().unwrap();
    let end = start.elapsed();
    println!("loaded: {}", gameinfo.armour.len());
    println!("time: {end:?}");
    // for name in ["Ablat", "Reflec", "Jack"] {
    //     let armour = gameinfo.armour.iter().find(|armour| armour.name == name).unwrap();
    //     println!("armour type: {}", armour.name);
    //     for dtype in [DamageType::Laser, DamageType::Melee] {
    //         let mut armour = armour.clone();
    //         println!("  damage type: {dtype:?}");
    //         println!("     before hit: {:?}", armour.protection_level(&gameinfo.engine, Character::default(), dtype));
    //         println!("    hit success: {:?}", armour.on_hit(&gameinfo.engine, Character::default(), dtype));
    //         println!("      after hit: {:?}", armour.protection_level(&gameinfo.engine, Character::default(), dtype));
    //     }
    // }
    // for armour in &gameinfo.armour {
    //     let mut armour = armour.clone();
    //     println!("name: {}", armour.name);
    //     // armour.status_bonus(&gameinfo.engine, Character::default()),
    //     // armour.mass(&gameinfo.engine, Character::default()),
    //     println!(
    //         "  powered: {:?}",
    //         armour.run_script_fn::<bool>(&gameinfo.engine, Character::default(), "is_powered"),
    //     );
    //     println!("  mass: {:?}", armour.mass(&gameinfo.engine, Character::default()));
    //     println!(
    //         "  power on? {:?}",
    //         armour.run_script_fn::<()>(&gameinfo.engine, Character::default(), "power_on"),
    //     );
    //     println!("  mass: {:?}", armour.mass(&gameinfo.engine, Character::default()));
    //     println!(
    //         "  power off? {:?}",
    //         armour.run_script_fn::<()>(&gameinfo.engine, Character::default(), "power_off"),
    //     );
    //     println!("  mass: {:?}", armour.mass(&gameinfo.engine, Character::default()));
    // }
}

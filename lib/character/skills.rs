use crate::{character::SkillLevel, def_is_enum};
use paste::paste;
use serde::{
    de::{Error, Visitor},
    Deserialize, Deserializer, Serialize,
};
use std::num::NonZeroU8;

#[derive(Clone, Debug, Default)]
pub struct Skills {
    admin: SkillLevel,
    advocate: SkillLevel,
    animals: [AnimalsSkill; 2],
    athletics: [AthleticsSkill; 3],
    art: [ArtSkill; 2],
    astrogation: SkillLevel,
    battle_dress: SkillLevel,
    broker: SkillLevel,
    carouse: SkillLevel,
    comms: SkillLevel,
    computers: SkillLevel,
    deception: SkillLevel,
    diplomat: SkillLevel,
    drive: [DriveSkill; 2],
    engineer: [EngineerSkill; 2],
    explosives: SkillLevel,
    flyer: [FlyerSkill; 2],
    gambler: SkillLevel,
    gunner: [GunnerSkill; 2],
    gun_combat: [GunCombatSkill; 3],
    heavy_weapons: [HeavyWeaponsSkill; 2],
    investigate: SkillLevel,
    jack_of_all_trades: SkillLevel,
    language: [LanguageSkill; 2],
    leadership: SkillLevel,
    life_sciences: [LifeSciencesSkill; 2],
    mechanic: SkillLevel,
    medic: SkillLevel,
    melee: [MeleeSkill; 2],
    navigation: SkillLevel,
    persuade: SkillLevel,
    pilot: [PilotSkill; 2],
    physical_sciences: [PhysicalSciencesSkill; 2],
    recon: SkillLevel,
    remote_operations: SkillLevel,
    seafarer: [SeafarerSkill; 2],
    sensors: SkillLevel,
    social_sciences: [SocialSciencesSkill; 2],
    space_sciences: [SpaceSciencesSkill; 2],
    stealth: SkillLevel,
    steward: SkillLevel,
    streetwise: SkillLevel,
    survival: SkillLevel,
    tactics: [TacticsSkill; 2],
    trade: [TradeSkill; 2],
    vacc_suit: SkillLevel,
    zero_g: SkillLevel,
}

macro_rules! specialised_skill {
    (
        $(#[$($itemmeta:tt)+])*
        $vis:vis $skillname:ident {
            $(
                $(#[$($fieldmeta:tt)+])*
                $specialty:ident$(($($field:ty),+$(,)?))?
            ),+$(,)?
        }
    ) => {
        paste! {
            $(#[$($itemmeta)+])*
            #[derive(Default)]
            $vis enum [<$skillname Specialty>] {
                $(
                    $(#[$($fieldmeta)+])*
                    $specialty$(($($field,)+))?,
                )+
            }

            $(#[$($itemmeta)+])*
            #[derive(Default)]
            $vis enum [<$skillname Skill>] {
                #[default]
                Untrained,
                Competent,
                Specialised {
                    specialty: [<$skillname Specialty>],
                    level: NonZeroU8,
                },
            }

            impl [<$skillname Skill>] {
                #[must_use]
                $vis fn dice_modifier(&self) -> i8 {
                    use [<$skillname Skill>]::*;
                    match self {
                        Untrained => -3,
                        Competent => 0,
                        Specialised { level, .. } => u8::from(*level) as i8,
                    }
                }

                #[must_use]
                $vis fn increase_level(&mut self) -> Option<&mut [<$skillname Specialty>]> {
                    use [<$skillname Skill>]::*;
                    match self {
                        Untrained => *self = Competent,
                        Competent => *self = Specialised {
                            specialty: [<$skillname Specialty>]::default(),
                            level: NonZeroU8::MIN,
                        },
                        Specialised { level, .. } => *level = level.saturating_add(1),
                    }
                    self.specialty_mut()
                }

                #[must_use]
                fn specialty_mut(&mut self) -> Option<&mut [<$skillname Specialty>]> {
                    use [<$skillname Skill>]::*;
                    match self {
                        Untrained | Competent => None,
                        Specialised { specialty, .. } => Some(specialty),
                    }
                }
            }
        }
    }
}

specialised_skill! {
    #[derive(Clone, Copy, Debug)]
    pub Animals {
        #[default]
        Riding,
        Veterinary,
        Training,
        Farming,
    }
}

specialised_skill! {
    #[derive(Clone, Copy, Debug)]
    pub Athletics {
        #[default]
        Coordination,
        Endurance,
        Strength,
        Flying,
    }
}

specialised_skill! {
    #[derive(Clone, Copy, Debug)]
    pub Art {
        #[default]
        Acting,
        Dance,
        Holography,
        Instrument,
        Sculpting,
        Writing,
    }
}

specialised_skill! {
    #[derive(Clone, Copy, Debug)]
    pub Drive {
        #[default]
        Mole,
        Tracked,
        Wheeled,
    }
}

specialised_skill! {
    #[derive(Clone, Copy, Debug)]
    pub Engineer {
        #[default]
        ManoeuvreDrive,
        JumpDrive,
        Electronics,
        LifeSupport,
        Power,
    }
}

specialised_skill! {
    #[derive(Clone, Copy, Debug)]
    pub Flyer {
        #[default]
        Grav,
        Rotor,
        Wing,
    }
}

specialised_skill! {
    #[derive(Clone, Copy, Debug)]
    pub Gunner {
        #[default]
        Turrets,
        Ortillery,
        Screens,
        CapitalWeapons,
    }
}

specialised_skill! {
    #[derive(Clone, Copy, Debug)]
    pub GunCombat {
        #[default]
        SlugRifle,
        SlugPistol,
        Shotgun,
        EnergyRifle,
        EnergyPistol,
    }
}

specialised_skill! {
    #[derive(Clone, Copy, Debug)]
    pub HeavyWeapons {
        #[default]
        Launchers,
        ManPortableArtillery,
        FieldArtillery,
    }
}

specialised_skill! {
    #[derive(Clone, Debug)]
    pub Language {
        #[default]
        Anglic,
        Vilani,
        Zdetl,
        Oynprith,
        Custom(String),
    }
}

specialised_skill! {
    #[derive(Clone, Copy, Debug)]
    pub LifeSciences {
        #[default]
        Biology,
        Cybernetics,
        Genetics,
        Psionicology,
    }
}

specialised_skill! {
    #[derive(Clone, Copy, Debug)]
    pub Melee {
        #[default]
        UnarmedCombat,
        Blade,
        Bludgeon,
        NaturalWeapons,
    }
}

specialised_skill! {
    #[derive(Clone, Copy, Debug)]
    pub Pilot {
        #[default]
        SmallCraft,
        Spacecraft,
        CapitalShips,
    }
}

specialised_skill! {
    #[derive(Clone, Copy, Debug)]
    pub PhysicalSciences {
        #[default]
        Physics,
        Chemistry,
        Electronics,
    }
}

specialised_skill! {
    #[derive(Clone, Copy, Debug)]
    pub Seafarer {
        #[default]
        Sail,
        Submarine,
        OceanShips,
        Motorboats,
    }
}

specialised_skill! {
    #[derive(Clone, Copy, Debug)]
    pub SocialSciences {
        #[default]
        Archeology,
        Economics,
        History,
        Linguistics,
        Philosophy,
        Psychology,
        Sophontology,
    }
}

specialised_skill! {
    #[derive(Clone, Copy, Debug)]
    pub SpaceSciences {
        #[default]
        Planetology,
        Robotics,
        Xenology,
    }
}

specialised_skill! {
    #[derive(Clone, Copy, Debug)]
    pub Tactics {
        #[default]
        MilitaryTactics,
        NavalTactics,
    }
}

specialised_skill! {
    #[derive(Clone, Copy, Debug)]
    pub Trade {
        #[default]
        Biologicals,
        CivilEngineering,
        SpaceConstruction,
        Hydroponics,
        Polymers,
    }
}

def_is_enum! {
    meta: [derive(Clone, Debug, Deserialize, Serialize)],
    name: SkillName,
    input: [
        Admin,
        Advocate,
        AnimalsRiding,
        AnimalsVeterinary,
        AnimalsTraining,
        AnimalsFarming,
        AthleticsCoordination,
        AthleticsEndurance,
        AthleticsStrength,
        AthleticsFlying,
        ArtActing,
        ArtDance,
        ArtHolography,
        ArtInstrument,
        ArtSculpting,
        ArtWriting,
        Astrogation,
        BattleDress,
        Broker,
        Carouse,
        Comms,
        Computers,
        Deception,
        Diplomat,
        DriveMole,
        DriveTracked,
        DriveWheeled,
        EngineerManoeuvreDrive,
        EngineerJumpDrive,
        EngineerElectronics,
        EngineerLifeSupport,
        EngineerPower,
        Explosives,
        FlyerGrav,
        FlyerRotor,
        FlyerWing,
        Gambler,
        GunnerTurrets,
        GunnerOrtillery,
        GunnerScreens,
        GunnerCapitalWeapons,
        GunCombatSlugRifle,
        GunCombatSlugPistol,
        GunCombatShotgun,
        GunCombatEnergyRifle,
        GunCombatEnergyPistol,
        HeavyWeaponsLaunchers,
        HeavyWeaponsManPortableArtillery,
        HeavyWeaponsFieldArtillery,
        Investigate,
        JackOfAllTrades,
        LanguageAnglic,
        LanguageVilani,
        LanguageZdetl,
        LanguageOynprith,
        LanguageCustom(String),
        Leadership,
        LifeSciencesBiology,
        LifeSciencesCybernetics,
        LifeSciencesGenetics,
        LifeSciencesPsionicology,
        Mechanic,
        Medic,
        MeleeUnarmedCombat,
        MeleeBlade,
        MeleeBludgeon,
        MeleeNaturalWeapons,
        Navigation,
        Persuade,
        PilotSmallCraft,
        PilotSpacecraft,
        PilotCapitalShips,
        PhysicalSciencesPhysics,
        PhysicalSciencesChemistry,
        PhysicalSciencesElectronics,
        Recon,
        RemoteOperations,
        SeafarerSail,
        SeafarerSubmarine,
        SeafarerOceanShips,
        SeafarerMotorboats,
        Sensors,
        SocialSciencesArcheology,
        SocialSciencesEconomics,
        SocialSciencesHistory,
        SocialSciencesLinguistics,
        SocialSciencesPhilosophy,
        SocialSciencesPsychology,
        SocialSciencesSophontology,
        SpaceSciencesPlanetology,
        SpaceSciencesRobotics,
        SpaceSciencesXenology,
        Stealth,
        Steward,
        Streetwise,
        Survival,
        TacticsMilitaryTactics,
        TacticsNavalTactics,
        TradeBiologicals,
        TradeCivilEngineering,
        TradeSpaceConstruction,
        TradeHydroponics,
        TradePolymers,
        VaccSuit,
        ZeroG,
    ],
    fields: [],
    is_fns: [],
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct RequiredSkill {
    name: SkillName,
    #[serde(deserialize_with = "deserialize_level")]
    level: u8,
}

impl RequiredSkill {
    #[must_use]
    pub fn new(name: SkillName, level: u8) -> Self {
        RequiredSkill { name, level }
    }
}

fn deserialize_level<'de, D>(d: D) -> Result<u8, D::Error>
where
    D: Deserializer<'de>,
{
    struct LevelVisitor;

    impl<'de> Visitor<'de> for LevelVisitor {
        type Value = u8;

        fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
            formatter.write_str("a valid trained skill level (any number in [0, 16))")
        }

        fn visit_u8<E>(self, v: u8) -> Result<Self::Value, E>
        where
            E: Error,
        {
            if (0..16).contains(&v) {
                Ok(v)
            } else {
                Err(E::custom(format!(
                    "invalid required skill level: {v}. must be in [0, 16)"
                )))
            }
        }
    }

    d.deserialize_u8(LevelVisitor)
}

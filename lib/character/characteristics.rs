use rhai::Engine;

#[repr(transparent)]
#[derive(Clone, Copy, Debug)]
pub struct Characteristic {
    val: u8,
}

impl Characteristic {
    /// Constructs a new `Characteristic`.
    ///
    /// # Errors
    ///
    /// This function returns an error with the input `val` if `val` is not in the range `0..16`.
    pub const fn new(val: u8) -> Result<Self, u8> {
        if val < 16 {
            Ok(Characteristic { val })
        } else {
            Err(val)
        }
    }

    #[must_use]
    pub const fn zero() -> Self {
        Characteristic { val: 0 }
    }

    #[must_use]
    pub const fn dice_modifier(&self) -> i8 {
        match self.val {
            0 => -3,
            1..=2 => -2,
            3..=5 => -1,
            6..=8 => 0,
            9..=11 => 1,
            12..=14 => 2,
            15 => 3,
            _ => unreachable!(),
        }
    }
}

#[derive(Clone, Copy, Debug)]
pub struct Characteristics {
    str: Characteristic,
    dex: Characteristic,
    end: Characteristic,
    int: Characteristic,
    edu: Characteristic,
    soc: Characteristic,
}

impl Characteristics {
    #[must_use]
    pub fn new() -> Self {
        Characteristics::default()
    }
}

impl Default for Characteristics {
    fn default() -> Self {
        Characteristics {
            str: Characteristic::zero(),
            dex: Characteristic::zero(),
            end: Characteristic::zero(),
            int: Characteristic::zero(),
            edu: Characteristic::zero(),
            soc: Characteristic::zero(),
        }
    }
}

#[derive(Clone, Copy, Debug, Default)]
pub struct StatusBonus {
    str: i8,
    dex: i8,
    end: i8,
    int: i8,
    edu: i8,
    soc: i8,
}

impl StatusBonus {
    #[allow(clippy::cast_possible_truncation, clippy::cast_sign_loss)]
    fn strength(&mut self, str: i64) {
        self.str = str as i8;
    }

    #[allow(clippy::cast_possible_truncation, clippy::cast_sign_loss)]
    fn dexterity(&mut self, dex: i64) {
        self.dex = dex as i8;
    }

    #[allow(clippy::cast_possible_truncation, clippy::cast_sign_loss)]
    fn endurance(&mut self, end: i64) {
        self.end = end as i8;
    }

    #[allow(clippy::cast_possible_truncation, clippy::cast_sign_loss)]
    fn intelligence(&mut self, int: i64) {
        self.int = int as i8;
    }

    #[allow(clippy::cast_possible_truncation, clippy::cast_sign_loss)]
    fn education(&mut self, edu: i64) {
        self.edu = edu as i8;
    }

    #[allow(clippy::cast_possible_truncation, clippy::cast_sign_loss)]
    fn society(&mut self, soc: i64) {
        self.soc = soc as i8;
    }

    pub fn register(engine: &mut Engine) {
        engine
            .register_type_with_name::<StatusBonus>("StatusBonus")
            .register_fn("new_status_bonus", StatusBonus::default)
            .register_fn("strength", StatusBonus::strength)
            .register_fn("dexterity", StatusBonus::dexterity)
            .register_fn("endurance", StatusBonus::endurance)
            .register_fn("intelligence", StatusBonus::intelligence)
            .register_fn("education", StatusBonus::education)
            .register_fn("society", StatusBonus::society);
    }
}

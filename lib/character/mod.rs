use crate::equipment::armour::LoadedArmour;
use characteristics::Characteristics;
use serde::{Deserialize, Serialize};
use skills::Skills;

pub mod career_history;
pub mod characteristics;
pub mod finances;
pub mod skills;

#[derive(Clone, Copy, Debug, Default, Deserialize, Serialize)]
pub enum SkillLevel {
    #[default]
    Untrained,
    Trained(u8),
}

impl SkillLevel {
    #[allow(clippy::cast_possible_wrap)]
    #[must_use]
    pub fn bonus(&self) -> i8 {
        match self {
            SkillLevel::Untrained => -3,
            SkillLevel::Trained(lv) => *lv as i8,
        }
    }

    pub fn increase_level(&mut self) {
        match self {
            SkillLevel::Untrained => *self = SkillLevel::Trained(0),
            SkillLevel::Trained(15) => {}
            SkillLevel::Trained(lv) => *lv += 1,
        }
    }

    /// Returns `true` if setting the level to `lv` succeeded, and `false` if it did not.
    pub fn set_level(&mut self, lv: Option<u8>) -> bool {
        match lv {
            Some(lv) if lv < 16 => {
                *self = SkillLevel::Trained(lv);
                true
            }
            Some(_) => false,
            None => {
                *self = SkillLevel::Untrained;
                true
            }
        }
    }
}

#[derive(Clone, Debug, Default)]
pub struct Character {
    name: String,
    age: u16,
    homeworld: (),
    race: (),
    characteristics: Characteristics,
    skills: Skills,
    finances: (),
    equipped_armour: Option<LoadedArmour>,
    owned_armour: Vec<LoadedArmour>,
    weapons: (),
}

#![recursion_limit = "256"]

use character::{characteristics::StatusBonus, skills::SkillName, Character};
use equipment::{
    armour::{Armour, LoadedArmour},
    DamageType,
};
use rhai::{
    plugin::{PluginFunction, RhaiResult},
    CallFnOptions, Dynamic, Engine, EvalAltResult, Module, NativeCallContext, ParseError, Scope,
    AST,
};
use ron::error::SpannedError;
use serde::{Deserialize, Serialize};
use std::{
    any::TypeId,
    fs::File,
    io::{BufReader, Error as IoError},
    marker::PhantomData,
    path::Path,
};

pub mod character;
pub mod equipment;

#[derive(Clone, Debug, Deserialize, Serialize)]
pub enum Scriptable<T> {
    Fixed(T),
    Scripted(String),
}

impl<T: Clone + Default + 'static> Scriptable<T> {
    /// Loads a scriptable item. If `script_file_ast` is `Some(ast)`, then the `String` in
    /// `Scriptable::Scripted` is treated as a function name to call for that scriptable item. If
    /// `script_file_ast` is `None`, then the `String` is treated as an evaluatable expression which
    /// will be called for that scriptable value.
    ///
    /// # Errors
    ///
    /// A [`GameError::RhaiError`] may be returned if:
    ///
    /// - The script file does not have an `init` function
    /// - The `init` function call fails
    /// - The standalone expression does not compile
    pub fn load(
        self,
        engine: &Engine,
        script_file_ast: Option<(&str, &AST)>,
    ) -> Result<LoadedScriptable<T>, GameError> {
        match self {
            Scriptable::Fixed(val) => Ok(LoadedScriptable::Fixed(val)),
            Scriptable::Scripted(string) => {
                if let Some((name, ast)) = script_file_ast {
                    let mut scope = Scope::new();
                    let state = engine.call_fn_with_options::<Dynamic>(
                        CallFnOptions::default().rewind_scope(false),
                        &mut scope,
                        ast,
                        &format!("init_{name}"),
                        (),
                    )?;
                    Ok(LoadedScriptable::FunctionName {
                        state: state.into_shared(),
                        name: string,
                    })
                } else {
                    let ast = engine.compile_expression(string)?;
                    Ok(LoadedScriptable::Standalone {
                        state: Dynamic::from(T::default()).into_shared(),
                        ast,
                    })
                }
            }
        }
    }
}

impl<T> TryFrom<LoadedScriptable<T>> for Scriptable<T> {
    type Error = GameError;

    fn try_from(value: LoadedScriptable<T>) -> Result<Self, Self::Error> {
        match value {
            LoadedScriptable::Fixed(val) => Ok(Scriptable::Fixed(val)),
            LoadedScriptable::Standalone { ast, .. } => match ast.source() {
                Some(source) => Ok(Scriptable::Scripted(source.to_string())),
                None => Err(GameError::MissingSource(ast)),
            },
            LoadedScriptable::FunctionName { name, .. } => Ok(Scriptable::Scripted(name)),
        }
    }
}

#[derive(Clone, Debug)]
pub enum LoadedScriptable<T> {
    Fixed(T),
    Standalone { state: Dynamic, ast: AST },
    FunctionName { state: Dynamic, name: String },
}

impl<T: Clone + 'static> LoadedScriptable<T> {
    pub fn state(&self) -> Option<&Dynamic> {
        match &self {
            LoadedScriptable::Fixed(_) => None,
            LoadedScriptable::Standalone { state, .. }
            | LoadedScriptable::FunctionName { state, .. } => Some(state),
        }
    }

    fn get_state(&mut self) -> Dynamic {
        match self {
            LoadedScriptable::Fixed(..) => Dynamic::UNIT,
            LoadedScriptable::Standalone { state, .. }
            | LoadedScriptable::FunctionName { state, .. } => state.clone(),
        }
    }

    pub fn register(engine: &mut Engine) {
        let name = format!("LoadedScriptable{}", std::any::type_name::<T>());
        engine
            .register_type_with_name::<LoadedScriptable<T>>(&name)
            .register_static_module(&name, rhai_module_generate::<T>().into());
    }
}

struct GetStateToken<T> {
    _t: PhantomData<T>,
}

impl<T> GetStateToken<T> {
    fn new() -> Self {
        GetStateToken { _t: PhantomData }
    }
}

impl<T: Clone + 'static> PluginFunction for GetStateToken<T> {
    #[inline]
    fn call(&self, _context: Option<NativeCallContext>, args: &mut [&mut Dynamic]) -> RhaiResult {
        let arg_0 = &mut args[0].write_lock::<LoadedScriptable<T>>().unwrap();
        Ok(Dynamic::from(arg_0.get_state()))
    }

    #[inline]
    fn is_method_call(&self) -> bool {
        true
    }

    #[inline]
    fn is_pure(&self) -> bool {
        true
    }

    #[inline]
    fn has_context(&self) -> bool {
        false
    }
}

fn rhai_module_generate<T: Clone + 'static>() -> Module {
    let mut m = Module::new();
    m.set_fn(
        "get$state",
        rhai::FnNamespace::Global,
        rhai::FnAccess::Public,
        None,
        [TypeId::of::<LoadedScriptable<T>>()],
        GetStateToken::<T>::new().into(),
    );
    m.set_fn(
        "set$state",
        rhai::FnNamespace::Global,
        rhai::FnAccess::Public,
        None,
        [TypeId::of::<LoadedScriptable<T>>()],
        GetStateToken::<T>::new().into(),
    );
    m.build_index();
    m
}

#[derive(Debug)]
pub struct GameInfo {
    pub engine: Engine,
    pub armour: Vec<LoadedArmour>,
}

impl GameInfo {
    /// Initialises the game information.
    ///
    /// # Errors
    ///
    /// A call to [`GameInfo::load_armour_file`] may fail for the argument `"default/armour.ron"`
    /// if:
    ///
    /// - `armour.ron` is missing
    /// - any of the `.rhai` files for the scripted armour types are missing
    pub fn init() -> Result<Self, GameError> {
        let mut engine = Engine::new();
        engine.register_type_with_name::<Character>("Character");
        LoadedArmour::register(&mut engine);
        SkillName::register_with_is_methods(&mut engine);
        DamageType::register_with_is_methods(&mut engine);
        StatusBonus::register(&mut engine);
        let mut gameinfo = GameInfo {
            engine,
            armour: Vec::new(),
        };
        gameinfo.load_armour_file("default/armour.ron")?;
        Ok(gameinfo)
    }

    /// Load in the armours defined in the file at the specified path.
    ///
    /// # Errors
    ///
    /// The following errors may occur in this function:
    ///
    /// - [`GameError::IoError`]: occurs on any I/O errors that may occur from a call to
    /// [`File::open`](std::fs::File::open).
    /// - [`GameError::RonError`]: occurs for any errors from a call to [`ron::de::from_reader`].
    /// - [`GameError::DuplicateName`]: occurs if the file contains any duplicate names, or if
    /// `self.armour` contains an instance of `LoadedArmour` with the same name as one being loaded
    /// by this function.
    /// - Any errors that may return from [`Armour::load`](crate::equipment::armour::Armour::load)
    pub fn load_armour_file<P: AsRef<Path>>(&mut self, path: P) -> Result<(), GameError> {
        let file = File::open(path)?;
        let reader = BufReader::new(file);
        let armour: Vec<Armour> = ron::de::from_reader(reader)?;
        let mut armour_final = Vec::with_capacity(armour.len());
        for armour in armour {
            if self
                .armour
                .iter()
                .chain(armour_final.iter())
                .any(|a2| a2.name == armour.name)
            {
                return Err(GameError::DuplicateName(armour.name));
            }
            let res = armour.load(&self.engine);
            // if let Some(name) = res.as_ref().ok().map(|armour| armour.name.as_str()) {
            //     println!("loaded armour: {name}");
            // }
            armour_final.push(res?);
        }
        self.armour.append(&mut armour_final);
        Ok(())
    }
}

#[derive(Debug)]
pub enum GameError {
    IoError(IoError),
    RonError(SpannedError),
    RhaiError(Box<EvalAltResult>),
    RhaiParseError(ParseError),
    MissingInit(String, Box<EvalAltResult>),
    DuplicateName(String),
    MissingSource(AST),
    NoScriptFile { name: String, fnname: String },
    MismatchedTypes { expected: String, found: String },
}

impl From<IoError> for GameError {
    fn from(value: IoError) -> Self {
        GameError::IoError(value)
    }
}

impl From<SpannedError> for GameError {
    fn from(value: SpannedError) -> Self {
        GameError::RonError(value)
    }
}

impl From<Box<EvalAltResult>> for GameError {
    fn from(value: Box<EvalAltResult>) -> Self {
        GameError::RhaiError(value)
    }
}

impl From<ParseError> for GameError {
    fn from(value: ParseError) -> Self {
        GameError::RhaiParseError(value)
    }
}

macro_rules! def_is_enum {
    (
        meta: [$($ometa:meta)*],
        name: $enum:ident,
        input: [$(#[$($imeta:meta)+])*$name:ident$(,$($rest:tt)*)?],
        fields: [$($field:tt)*],
        is_fns: [$($fndef:tt)*],
    ) => {
        paste::paste!{
            def_is_enum! {
                meta: [$($ometa)*],
                name: $enum,
                input: [$($($rest)*)?],
                fields: [$($field)* [[$([$($imeta)+])*] $name]],
                is_fns: [$($fndef)* [[<is_ $name:snake>], $enum::$name]],
            }
        }
    };
    (
        meta: [$($ometa:meta)*],
        name: $enum:ident,
        input: [$(#[$($imeta:meta)+])*$name:ident($($inner:tt)*)$(,$($rest:tt)*)?],
        fields: [$($field:tt)*],
        is_fns: [$($fndef:tt)*],
    ) => {
        paste::paste! {
            def_is_enum! {
                meta: [$($ometa)*],
                name: $enum,
                input: [$($($rest)*)?],
                fields: [$($field)* [[$([$($imeta)+])*] $name($($inner)*)]],
                is_fns: [$($fndef)* [[<is_ $name:snake>], $enum::$name(..)]],
            }
        }
    };
    (
        meta: [$($ometa:meta)*],
        name: $enum:ident,
        input: [$(#[$($imeta:meta)+])*$name:ident { $($inner:tt)* }$(,$($rest:tt)*)?],
        fields: [$($field:tt)*],
        is_fns: [$($fndef:tt)*],
    ) => {
        paste::paste!{
            def_is_enum! {
                meta: [$($ometa)*],
                name: $enum,
                input: [$($($rest)*)?],
                fields: [$($field)* [[$([$($imeta)+])*] $name { $($inner)* }]],
                is_fns: [$($fndef)* [[<is_ $name:snake>], $enum::$name { .. }]],
            }
        }
    };
    (
        meta: [$($ometa:meta)*],
        name: $enum:ident,
        input: [],
        fields: [$([[$([$($imeta:tt)+])*] $($inner:tt)+])*],
        is_fns: [$([$name:ident, $pat:pat_param])*],
    ) => {
        $(#[$ometa])*
        pub enum $enum {
            $($(#[$($imeta)+])*$($inner)+,)*
        }

        impl $enum {
            pub fn register_with_is_methods(engine: &mut rhai::Engine) {
                engine
                    .register_type_with_name::<$enum>(stringify!($enum))
                    $(
                        .register_fn(stringify!($name), |t: $enum| matches!(t, $pat))
                    )*;
            }
        }
    }
}

pub(crate) use def_is_enum;

use crate::{
    character::{
        characteristics::StatusBonus,
        skills::{RequiredSkill, SkillName},
        Character,
    },
    equipment::DamageType,
    GameError, LoadedScriptable, Scriptable,
};
use rhai::{CallFnOptions, Dynamic, Engine, EvalAltResult, Scope, AST};
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Armour {
    pub name: String,
    tech_level: u8,
    protection: Scriptable<u8>,
    required_skill: Option<RequiredSkill>,
    cost: u32,
    mass: Scriptable<f32>,
    pub script_file: Option<String>,
    description: String,
}

impl Armour {
    #[must_use]
    #[allow(clippy::too_many_arguments)]
    pub fn new<T, U, V>(
        name: &str,
        tech_level: u8,
        protection: Scriptable<u8>,
        required_skill: Option<RequiredSkill>,
        cost: u32,
        mass: Scriptable<f32>,
        script_file: Option<U>,
        description: V,
    ) -> Self
    where
        T: Into<String>,
        U: Into<String>,
        V: Into<String>,
    {
        Armour {
            name: name.into(),
            tech_level,
            protection,
            required_skill,
            cost,
            mass,
            script_file: script_file.map(Into::into),
            description: description.into(),
        }
    }

    /// Load a configuration scriptable into a runtime scriptable.
    ///
    /// # Errors
    ///
    /// May produce any errors that a call to [`Scriptable::load`] can, though
    /// `GameError::RhaiError(EvalAltResult("init", _))` is mapped to `GameError::MissingInit`.
    pub fn load(self, engine: &Engine) -> Result<LoadedArmour, GameError> {
        let armour = match self.script_file {
            Some(path) => {
                let ast = engine.compile_file(path.as_str().into())?;
                let protection = self
                    .protection
                    .load(engine, Some(("protection", &ast)))
                    .map_err(|err| match err {
                        GameError::RhaiError(err) => match err.as_ref() {
                            EvalAltResult::ErrorFunctionNotFound(name, _)
                                if name.starts_with("init") =>
                            {
                                GameError::MissingInit(self.name.clone(), err)
                            }
                            _ => GameError::RhaiError(err),
                        },
                        other => other,
                    })?;
                let mass = self
                    .mass
                    .load(engine, Some(("mass", &ast)))
                    .map_err(|err| match err {
                        GameError::RhaiError(err) => match err.as_ref() {
                            EvalAltResult::ErrorFunctionNotFound(name, _)
                                if name.starts_with("init_") =>
                            {
                                GameError::MissingInit(self.name.clone(), err)
                            }
                            _ => GameError::RhaiError(err),
                        },
                        other => other,
                    })?;
                LoadedArmour {
                    name: self.name,
                    tech_level: self.tech_level,
                    protection,
                    required_skill: self.required_skill,
                    cost: self.cost,
                    mass,
                    script_ast: Some((ast, path)),
                    description: self.description,
                }
            }
            None => LoadedArmour {
                name: self.name,
                tech_level: self.tech_level,
                protection: self.protection.load(engine, None)?,
                required_skill: self.required_skill,
                cost: self.cost,
                mass: self.mass.load(engine, None)?,
                script_ast: None,
                description: self.description,
            },
        };
        Ok(armour)
    }
}

impl TryFrom<LoadedArmour> for Armour {
    type Error = GameError;

    fn try_from(value: LoadedArmour) -> Result<Self, Self::Error> {
        Ok(Armour {
            name: value.name,
            tech_level: value.tech_level,
            protection: value.protection.try_into()?,
            required_skill: value.required_skill,
            cost: value.cost,
            mass: value.mass.try_into()?,
            script_file: value.script_ast.map(|pair| pair.1),
            description: value.description,
        })
    }
}

#[allow(clippy::module_name_repetitions)]
#[derive(Clone, Debug)]
pub struct LoadedArmour {
    pub name: String,
    tech_level: u8,
    protection: LoadedScriptable<u8>,
    required_skill: Option<RequiredSkill>,
    cost: u32,
    mass: LoadedScriptable<f32>,
    pub script_ast: Option<(AST, String)>,
    description: String,
}

impl LoadedArmour {
    pub(crate) fn register(engine: &mut Engine) {
        LoadedScriptable::<u8>::register(engine);
        LoadedScriptable::<f32>::register(engine);
        engine
            .register_type_with_name::<LoadedArmour>("Armour")
            .register_get("mass", |la: &mut LoadedArmour| la.mass.clone());
    }

    #[allow(clippy::cast_lossless)]
    /// Determine whether a skill check against this armour succeeds.
    ///
    /// # Errors
    ///
    /// May return any error that a call to [`Engine::call_fn_with_options`] may produce except
    /// `EvalAltResult::ErrorFunctionNotFound`, which is mapped to `Ok(true)`.
    pub fn on_skill_check(
        &self,
        engine: &Engine,
        character: Character,
        skill: &SkillName,
        roll: u8,
    ) -> Result<bool, Box<rhai::EvalAltResult>> {
        match &self.script_ast {
            Some((ast, _)) => {
                let mut scope = Scope::new();
                scope.push("character", character);
                let result = engine.call_fn_with_options::<bool>(
                    CallFnOptions::default().rewind_scope(false),
                    &mut scope,
                    ast,
                    "on_skill_check",
                    (skill.clone(), roll as i64),
                );
                match result {
                    Err(err) => match err.as_ref() {
                        EvalAltResult::ErrorFunctionNotFound(..) => Ok(true),
                        _ => Err(err),
                    },
                    res => res,
                }
            }
            None => Ok(true),
        }
    }

    #[allow(clippy::cast_sign_loss, clippy::cast_possible_truncation, clippy::missing_panics_doc)]
    /// Get the current protection level of an armour instance.
    ///
    /// # Errors
    ///
    /// May return any error that a call to [`Engine::call_fn_with_options`] or
    /// [`Engine::eval_ast_with_scope`] may produce.
    pub fn protection_level(
        &self,
        engine: &Engine,
        character: Character,
        damage_type: DamageType,
    ) -> Result<u8, Box<rhai::EvalAltResult>> {
        match &self.protection {
            LoadedScriptable::Fixed(fixed) => Ok(*fixed),
            LoadedScriptable::Standalone { state, ast } => {
                let mut scope = Scope::new();
                scope.push("character", character);
                scope.push("state", state.clone());
                scope.push("damage_type", damage_type);
                engine
                    .eval_ast_with_scope::<i64>(&mut scope, ast)
                    .map(|res| res as u8)
            }
            LoadedScriptable::FunctionName { state, name } => {
                let ast = self.script_ast.as_ref().map(|(ref ast, _)| ast).unwrap();
                let mut scope = Scope::new();
                scope.push("character", character);
                scope.push("state", state.clone());
                engine
                    .call_fn_with_options::<i64>(
                        CallFnOptions::default().rewind_scope(false),
                        &mut scope,
                        ast,
                        name,
                        (damage_type,),
                    )
                    .map(|res| res as u8)
            }
        }
    }

    /// Provides the opportunity to mutate the protection of this armour when a hit is received.
    ///
    /// # Errors
    ///
    /// May return any error that a call to [`Engine::call_fn_with_options`] may produce except
    /// `EvalAltResult::ErrorFunctionNotFound`, which is mapped to `Ok(())`.
    pub fn on_hit(
        &mut self,
        engine: &Engine,
        character: Character,
        damage_type: DamageType,
    ) -> Result<(), Box<rhai::EvalAltResult>> {
        match (&self.script_ast, &mut self.protection) {
            (Some((ast, _)), LoadedScriptable::FunctionName { state, .. }) => {
                let mut scope = Scope::new();
                scope.push("character", character);
                let res = engine.call_fn_with_options::<()>(
                    CallFnOptions::default()
                        .rewind_scope(false)
                        .bind_this_ptr(state),
                    &mut scope,
                    ast,
                    "on_hit",
                    (damage_type,),
                );
                match res {
                    Err(err) => match err.as_ref() {
                        EvalAltResult::ErrorFunctionNotFound(..) => Ok(()),
                        _ => Err(err),
                    },
                    other => other,
                }
            }
            _ => Ok(()),
        }
    }

    /// Get the status bonus from an armour type.
    ///
    /// # Errors
    ///
    /// May return any error that a call to [`Engine::call_fn_with_options`] may produce except
    /// `EvalAltResult::ErrorFunctionNotFound`, which is mapped to `Ok(StatusBonus::default())`.
    pub fn status_bonus(
        &self,
        engine: &Engine,
        character: Character,
    ) -> Result<StatusBonus, Box<EvalAltResult>> {
        match &self.script_ast {
            Some((ast, _)) => {
                let mut scope = Scope::new();
                scope.push("character", character);
                let res = engine.call_fn_with_options::<StatusBonus>(
                    CallFnOptions::default().rewind_scope(false),
                    &mut scope,
                    ast,
                    "status_bonus",
                    (),
                );
                match res {
                    Err(err) => match err.as_ref() {
                        EvalAltResult::ErrorFunctionNotFound(..) => Ok(StatusBonus::default()),
                        _ => Err(err),
                    },
                    other => other,
                }
            }
            _ => Ok(StatusBonus::default()),
        }
    }

    #[allow(clippy::missing_panics_doc)]
    /// Get the mass of an armour.
    ///
    /// # Errors
    ///
    /// If the mass is scriptable, then this function may error if:
    ///
    /// - Evaluating the standalone AST fails
    /// - Calling the mass function name fails
    /// - The returned type is not `f32`
    pub fn mass(&self, engine: &Engine, character: Character) -> Result<f32, Box<EvalAltResult>> {
        match &self.mass {
            LoadedScriptable::Fixed(mass) => Ok(*mass),
            LoadedScriptable::Standalone { state, ast } => {
                let mut scope = Scope::new();
                scope.push("character", character);
                scope.push("state", state.clone());
                engine.eval_ast_with_scope::<f32>(&mut scope, ast)
            }
            LoadedScriptable::FunctionName { state, name } => {
                let ast = self.script_ast.as_ref().map(|(ref ast, _)| ast).unwrap();
                let mut scope = Scope::new();
                scope.push("character", character);
                scope.push("state", state.clone());
                engine.call_fn_with_options::<f32>(
                    CallFnOptions::default().rewind_scope(false),
                    &mut scope,
                    ast,
                    name,
                    (),
                )
            }
        }
    }

    /// Run a named function from the compiled script AST contained in a given `LoadedArmour`
    /// instance.
    ///
    /// # Errors
    ///
    /// If this instance does not have a `script_ast`, then a `GameError::NoScript` file is
    /// returned.
    ///
    /// If the instance has a `script_ast`, then this function may return any errors from a call to
    /// the specified function. Additionally, if the specified function doesn't
    pub fn run_script_fn<T: Clone + 'static>(
        &mut self,
        engine: &Engine,
        character: Character,
        name: &str,
    ) -> Result<T, GameError> {
        if let Some((ast, _)) = self.script_ast.as_ref() {
            let mut scope = Scope::new();
            scope.push("character", character);
            let mut this = Dynamic::from(self.clone());
            let res = engine
                .call_fn_with_options::<T>(
                    CallFnOptions::default()
                        .rewind_scope(false)
                        .bind_this_ptr(&mut this),
                    &mut scope,
                    ast,
                    name,
                    (),
                )
                .map_err(GameError::from)?;
            if this.is::<Self>() {
                *self = this.cast();
                Ok(res)
            } else if this.is::<()>() {
                Ok(res)
            } else {
                Err(GameError::MismatchedTypes {
                    expected: "LoadedArmour".into(),
                    found: this.type_name().into(),
                })
            }
        } else {
            Err(GameError::NoScriptFile {
                name: self.name.clone(),
                fnname: name.to_string(),
            })
        }
    }
}

use crate::def_is_enum;
use serde::{Deserialize, Serialize};

pub mod armour;
pub mod computer;
pub mod weapons;

def_is_enum! {
    meta: [derive(Clone, Copy, Debug, Deserialize, Serialize)],
    name: DamageType,
    input: [
        Melee,
        Laser,
        Projectile,
        Explosive,
    ],
    fields: [],
    is_fns: [],
}

#[derive(Clone, Copy, Debug, Default, Deserialize, Serialize)]
pub struct StatBonus {
    str: i8,
    dex: i8,
    end: i8,
    int: i8,
    edu: i8,
    soc: i8,
}

impl StatBonus {
    #[must_use]
    pub fn new(str: i8, dex: i8, end: i8, int: i8, edu: i8, soc: i8) -> Self {
        StatBonus {
            str,
            dex,
            end,
            int,
            edu,
            soc,
        }
    }
}

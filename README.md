A reimplementation of the rules of the Traveller TTRPG (Mongoose, 2008). Currently work is going
into the `lib/` portion of the project where most of the functionality will be, but I do plan on
creating a user interface of some sort (TUI or GUI) in `bin/` once I'm done with the library
portion. Due to the game being OGL and having no Open Content I am unable to publish
reimplementations of the game's items, races, etc. in this repository, however it shouldn't be
too difficult to create them yourself from the source book by following the provided examples in the
`examples/` dir. I'll write proper documentation on how to create homebrew things for this game once
the actual API for scripting is settled out more firmly.

Current task list:

- Armour
  - Implement options
  - Implement augments
  - Implement computers so certain armours from the base game can provide them
    - Decide on an API for this
- Computers
- Weapons
- Environments
- Worlds
- Interactions
  - Everything needs to have some way to provide a DM to skill checks
